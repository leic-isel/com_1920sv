%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% one_time_pad_decipher.m
%  
%

function one_time_pad_decipher( filename )
	% ONE_TIME_PAD_CIPHER
    % Receives a ciphered filename to decipher
    % Saves the file deciphered
    
    % Retrieve bits of data from file and from key ----------------------------
    ext = extractAfter (filename, '.'); % get extension    
    keyname = [extractBefore(filename, '_ciphered'), '.key'];

    bitfile = get_file_bits(filename); % bits from ciphered file
    bitkey = get_file_bits(keyname); % bits from cipher key
    
    % Decipher the file -------------------------------------------------------
    % XOR operate the bits from ciphered file and from cipher key
    decipheredbits = bitxor(bitfile, bitkey);
    
    % Save deciphered file ----------------------------------------------------
    fName = extractBefore (filename, '_ciphered'); % get original filename
    outFileName = [fName, '_deciphered.', ext];    
    write_file_data(outFileName, decipheredbits);
end


function bitvector = get_file_bits( filename )
    % GET_FILE_BITS
    % Receives a filename
    % Returns a bit vector containing the file data


    fileID = fopen( filename, 'rb' ); % open file for read
    
    if -1==fileID % Check descriptor.
        error('Error opening file %s for reading',filename);
    end

    data = fread( fileID, Inf, 'uchar' ); % read file to a vector
    
    fclose( fileID ); % close file
    
    data = data'; % Transpose to a row vector
    
    % get file bits
    k = 1;
    for i=1 : length(data)
        bitvector(k:k+7) = bitget( data(i), 8:-1:1 );
        k = k + 8;
    end
end


function write_file_data(filename, filedata)
    % WRITE_FILE_DATA
    % Receives filename and the filedata in bits
    % Saves the filedata to a file with the filename

    p2 = 2.^(7:-1:0); % vector with the powers of 2
   
    % vector with 8 bit integer elements
    ciphfile = uint8( zeros(1, length(filedata) / 8 ) );

    % point to point calculation
    k = 1;
    for i=1: length(ciphfile)
        ciphfile(i) = filedata( k:k+7 ) * p2';
        k = k + 8;
    end
    
    fileID = fopen( filename, 'wb'); % open file in write big Endian mode
    
    if -1==fileID % check descriptor
        error( 'Error opening file %s for writing ', filename );
    end
    
    fwrite( fileID, ciphfile, 'uchar' ); % write the vector to file

    fclose( fileID ); % close the file
end
