%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunica��es. 
%
% Trabalho Pr�tico - M�dulo 1
% 32398 H�lder Augusto
% 39619 F�bio Teixeira
% 45824 Nuno Ven�ncio
%
% entropy_histogram.m
%  
%

function entropy_histogram( filename )
    % ENTROPY_HISTOGRAM 
    % Receives a filename to compute the Entropy.
    % Prints the Entropy H(X) and generates an histogram of symbol 
    % ocurrences in the file. 
    
    %
    %----------------------------------------------------------------------
    % Retrieve data from file
    %----------------------------------------------------------------------
    %
    
    % Open file with permission 'rb' (r read, b Big-endian ordering)
    fileID = fopen( filename, 'rb' );

    % Check descriptor.
    if -1==fileID
        error('Error opening file %s for reading',filename);
    end

    % Read entire file to a vector.
    % fileID - File identifier
    % Inf - Column vector, with each element containing a value of the file
    % 'unchar' - precision (class and size of values to read), unchar is 8 bits
    data = fread( fileID, Inf, 'uchar' );

    % Transpose to a row vector.
    data = data';

    % Close file.
    fclose( fileID );

    %
    %----------------------------------------------------------------------
    % Manipulate data
    %----------------------------------------------------------------------
    %
    
    % Find the unique values of A and the index vectors ~ and iSymbols, 
    % such that Symbols = A(ia) and A = Symbols(iSymbols). 
    [Symbols, ~, iSymbols] = unique(data);

    % A = accumarray(subs,val) returns array A by accumulating elements of 
    % vector val using the subscripts subs. If subs is a column vector, then 
    % each element defines a corresponding subscript in the output, which is 
    % also a column vector. The accumarray function collects all elements of 
    % val that have identical subscripts in subs and stores their sum in the 
    % location of A corresponding to that subscript.
    ocurrences = accumarray(iSymbols,1);

    % Create new matrix with transpose Simbolos, symbol occurrences and 
    % symbol probabilities
    value_counts = [Symbols', ocurrences, ocurrences / length(data)];
    
    %
    % ---------------------------------------------------------------------
    % Entropy calculation
    % ---------------------------------------------------------------------
    %
    
    p = value_counts(:,3);
    H = -sum(p .* log2(p));
    fprintf('Entropia do ficheiro %s\nH(X)=%f\n', filename, H);

    %
    %----------------------------------------------------------------------
    % Histogram
    %----------------------------------------------------------------------
    %
    
    hist(data, 0:1:255);
    grid on;
    xlabel('S�mbolos');
    ylabel('Ocorr�ncias');
    title(['Histograma de ', filename]);
    
end
