%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunica��es. 
%
% Trabalho Pr�tico - M�dulo 1
% 32398 H�lder Augusto
% 39619 F�bio Teixeira
% 45824 Nuno Ven�ncio
%
% symbol_source.m
%  
%

function symbol_source( M_dimension, prob, outFilename )
    % SYMBOL_SOURCE 
    % receives dimension of a file, an array of probability mass function and
    % an output filename.
    % creates a file with dimension M_dimension (100, 1000 and 10000) with 
    % 5 symbols (A, E, I, O, U) with probabilities prob
    
    %
    %----------------------------------------------------------------------
    % Array creation
    %----------------------------------------------------------------------
    %
    
    % Symbol dimenssions
    aDim = round(M_dimension * prob(1));
    eDim = round(M_dimension * prob(2));
    iDim = round(M_dimension * prob(3));
    oDim = round(M_dimension * prob(4));
    uDim = round(M_dimension * prob(5));
    
    % Arrays of unique symbols
    A = 'A' .* ones(1,aDim);
    E = 'E' .* ones(1,eDim);
    I = 'I' .* ones(1,iDim);
    O = 'O' .* ones(1,oDim);
    U = 'U' .* ones(1,uDim);

    % Join arrays created
    finalArray = [A, E, I, O, U];
    
    % Mix values in array (para n�o ter os valores iguais todos seguidos, descomentar o bloco seguinte) 
%     temp = zeros(1, M_dimension, 'int8');
%     for i=1 : length(temp)
%         val = round(rand() * (length(finalArray)-1))+1; % -1 para n�o ultrapassar o tamanho da array qd somo 1 e +1 para nunca sair zero    
%         temp(i) = finalArray(val);
%         finalArray = [finalArray(1:val-1), finalArray(val+1:end)];
%     end
%     finalArray = [finalArray, temp];

    %
    %----------------------------------------------------------------------
    % File writing
    %----------------------------------------------------------------------
    %
    
    % Open file in write big Endian mode
    fileID = fopen( outFilename, 'wb');
    
    % Check descriptor
    if -1==fileID
        error( 'Error opening file %s for writing ', outFilename );
    end
    
    % Write the vector to file
    fwrite( fileID, finalArray, 'uchar' );
    
    % Close the file
    fclose( fileID );
end

