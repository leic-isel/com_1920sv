%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% hamming_encode.m
%  
%

function encoded = hamming_encode( filename )
    % HAMMING_ENCODE
    % Receives a filename
    % Computes de hamming encoding (7, 4)
    % Returns the bit vector encoded
    
    bitvector = get_file_bits(filename);
    len = length(bitvector);
    
    % builld encoded vector    
    %encoded = zeros(1, len + ceil(len/k));
    encoded = [];
    for i=1 : 4 : len       
        encoded = [encoded, encode(bitvector(i:i+3))];
    end
end

function n = encode(k)
    % ENCODE 
    % recives k, a vector of zeros and ones of the message
    % returns n, a vector of zeros and ones of the code word to transmit
    % after xor operate the bits in k

    b0 = xor(xor(k(2), k(2)), k(4));
    b1 = xor(xor(k(1), k(2)), k(4));
    b2 = xor(xor(k(1), k(3)), k(4));

    % append the result of XOR operations to the mensage    
    n = [k, b0, b1, b2];
end

function bitvector = get_file_bits( filename )
    % GET_FILE_BITS
    % Receives a filename
    % Returns a bit vector containing the file data

    fileID = fopen( filename, 'rb' ); % open file for read
    
    if -1==fileID % Check descriptor.
        error('Error opening file %s for reading',filename);
    end

    data = fread( fileID, Inf, 'uchar' ); % read file to a vector
    
    fclose( fileID ); % close file
    
    data = data'; % Transpose to a row vector
    
    % get file bits
    k = 1;
    for i=1 : length(data)
        bitvector(k:k+7) = bitget( data(i), 8:-1:1 );
        k = k + 8;
    end
end
