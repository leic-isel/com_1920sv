%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% parity_check_decode.m
%  
%

function [decoded, error] = parity_check_decode( bitstream, n )
    % PARITY_CHECK_DECODE
    % Receives a bit vector of encoded data and the length of the code word (n)
    % Returns the bit vector decoded and prints 'error' if any error is
    % detected
    
    % get the number of total parity bits
    total_parity_bits = length(bitstream)/n;
    
    % check how many bits we append to the last message word in encoding
    a = length(bitstream) - total_parity_bits;
    num_bits_appended = mod(a, n);
    
    % built decoded vector and get the number of errors
    decoded = [];
    error = 0;
    for i=1 : n : length(bitstream)-1
        [dec, error] = decode(bitstream(i: i+n-1));        
        decoded = [decoded, dec];
        if (error)
            disp('error');
        end
    end
    
    % remove bits appended in encoding
    decoded = decoded(1:length(decoded)-num_bits_appended);
end


function [msg_bits, has_error] = decode(n)
    % DECODE 
    % receives n, a code word vector
    % returns a vector with the message decoded and the boolean flag has_error
    % 0 no error, 1 error

    msg_bits = n(1:length(n)-1); % bits from the message
    parity_bit = n(end); % last bit

    for i=1 : length(msg_bits)-1  
        if i==1 % first iteraction, XOR operate 1st and 2nd bits
            xor_op = xor(msg_bits(i), msg_bits(i+1));
        else % next interactions, XOR operate previous result with next bit
            xor_op = xor(xor_op, msg_bits(i+1));
        end
    end
    
    % check for error
    has_error = xor(xor_op, parity_bit);
end