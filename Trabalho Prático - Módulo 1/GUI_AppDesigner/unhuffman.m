%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% unhuffman.m
%  
%

function bitdata = unhuffman( filedata, code )
    % UNHUFFMAN
    % Receives a bit filedata and an huffman code
    % Returns the data in bits decode

    % huffman decode
    bitdata = huffmandeco(double(filedata'), code);

    % set 8bit data in binary data
    bitdata = get_bits(bitdata');
end