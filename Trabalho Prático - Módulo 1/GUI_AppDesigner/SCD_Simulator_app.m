classdef SCD_Simulator_app < matlab.apps.AppBase

    % Properties that correspond to app components
    properties (Access = public)
        SCDSimuladorUIFigure         matlab.ui.Figure
        Image                        matlab.ui.control.Image
        SCDSimuladorGrupo01TurmaLI31NLabel  matlab.ui.control.Label
        ComunicaesTurmaLI31NGrupo01Label matlab.ui.control.Label
        FicheiroatransmitirPanel     matlab.ui.container.Panel
        AbrirButton                  matlab.ui.control.Button
        HistogramaButton             matlab.ui.control.Button
        EntropiaLabel                matlab.ui.control.Label
        Label                        matlab.ui.control.Label
        bitsmboloLabel               matlab.ui.control.Label
        bitsLabel                    matlab.ui.control.Label
        Label_2                      matlab.ui.control.Label
        UIAxes                       matlab.ui.control.UIAxes
        BEREditFieldLabel            matlab.ui.control.Label
        BEREditField                 matlab.ui.control.EditField
        CodificaodefonteButtonGroup  matlab.ui.container.ButtonGroup
        AusnciadecodificaoButton     matlab.ui.control.ToggleButton
        CdigodeHuffmanButton         matlab.ui.control.ToggleButton
        FormatoZIPButton             matlab.ui.control.ToggleButton
        CifraButtonGroup             matlab.ui.container.ButtonGroup
        SemcifraButton               matlab.ui.control.ToggleButton
        OnetimePadVernamButton       matlab.ui.control.ToggleButton
        TextoemclaroButton           matlab.ui.control.Button
        CriptogramaButton            matlab.ui.control.Button
        TextodecifradoButton         matlab.ui.control.Button
        CodificaodecanalButtonGroup  matlab.ui.container.ButtonGroup
        SemcontrolodeerrosButton     matlab.ui.control.ToggleButton
        Bitparidadepar87Button       matlab.ui.control.ToggleButton
        CdigoHamming74Button         matlab.ui.control.ToggleButton
        FicheirorecebidoPanel        matlab.ui.container.Panel
        AbrirButton_2                matlab.ui.control.Button
        bitsLabel_2                  matlab.ui.control.Label
        Label_3                      matlab.ui.control.Label
        bitsLabel_3                  matlab.ui.control.Label
        Label_4                      matlab.ui.control.Label
        BERdatransmissoLabel         matlab.ui.control.Label
        Label_5                      matlab.ui.control.Label
        SimularButton                matlab.ui.control.Button
        SairButton                   matlab.ui.control.Button
    end
    
    methods (Access = private)
        function decoded_data = decodeRX(app, hObject, eventdata, handles, encoded_data, code)
            decoded_data = encoded_data;
            
            % Channel decoding
            if get(handles.codcanalBitPPar, 'Value')
                decoded_data = parity_check_decode(decoded_data, 8);
            elseif get(handles.codcanalH74, 'Value')
                decoded_data = hamming_decode_correct(decoded_data);
            end
            
            % Decipher
            if get(handles.cifraVern, 'Value')
                decoded_data = one_time_pad_decipher(decoded_data, [handles.path, handles.file]);
            end
            
            % Source decoding
            if get(handles.codfonteHuffman, 'Value')
                decoded_data = unhuffman(decoded_data, code);
            elseif get(handles.codfonteZIP, 'Value')
                decoded_data = unzipfile();
            end
        end
        
        function [encoded_data, code] = encodeTX(app, hObject, eventdata, handles)
            encoded_data = get_bits(handles.data);
            code = 0;
            
            % check what radiobutton is selected in Codifica��o de fonte
            if get(handles.codfonteAusencia, 'Value')
                set(handles.text17, 'string', get(handles.text18, 'String'));
            elseif get(handles.codfonteHuffman, 'Value')
                [nBits, code, encoded_data] = huffman(encoded_data, handles.symbol_stats);
                set(handles.text17, 'string', num2str(nBits));
            else
                [zip_data, nbits] = zipfile([handles.path, handles.file]);
                encoded_data = get_bits(zip_data);
                set(handles.text17, 'string',num2str(nbits));
            end
            
            % check what radiobutton is selected in Cifra
            if get(handles.cifraVern, 'Value')
                encoded_data = one_time_pad_cipher(encoded_data, [handles.path, handles.file]);
                handles.ciphdata = encoded_data;
                guidata(hObject, handles);
            end
            
            % check what radiobutton is selected in Codifica��o de canal
            if get(handles.codcanalBitPPar, 'Value')
                encoded_data = parity_check_encode(encoded_data, 7);
            elseif get(handles.codcanalH74, 'Value')
                encoded_data = hamming_encode(encoded_data);
            end
            guidata(hObject, handles);
        end
        
        function encoded_data_with_errors = set_errors(app, hObject, eventdata, handles, encoded_data)
            [encoded_data_with_errors, total_errors] = ber(encoded_data, str2double(get(handles.berVal, 'string')));
            set(handles.text19, 'string', num2str(length(encoded_data_with_errors)));
            
            transmitedBER = num2str(total_errors/length(encoded_data_with_errors), '%10.4e');
            set(handles.text16, 'string', transmitedBER);
        end
        
    end
    
    % Callbacks that handle component events
    methods (Access = private)
         % Code that executes after component creation
        function SCD_Simulator_OpeningFcn(app, varargin)
            % Create GUIDE-style callback args - Added by Migration Tool
            [hObject, eventdata, handles] = convertToGUIDECallbackArguments(app); %#ok<ASGLU>
            
                % This function has no output args, see OutputFcn.
                % hObject    handle to figure
                % eventdata  reserved - to be defined in a future version of MATLAB
                % handles    structure with handles and user data (see GUIDATA)
                % varargin   command line arguments to SCD_Simulator (see VARARGIN)
            
                % Choose default command line output for SCD_Simulator
                handles.output = hObject;
            
                set(handles.codfonteAusencia,'value',1);
                set(handles.codcanalAusencia,'value',1);
            
                set(handles.simSCD,'Enable','Off');
                set(handles.fileHistogram,'Enable','Off');
                set(handles.abrirFileRX,'Enable','Off');
                set(handles.cifraTextoClaro,'Enable','Off');
                set(handles.cifraCriptograma,'Enable','Off');
                set(handles.cifraTextoDecifrado,'Enable','Off');
            
                handles.ciphdata = [];
                handles.rxdata = '';
            
                % Update handles structure
                guidata(hObject, handles);
        end

        % Button pushed function: abrirFileRX
        function abrirFileRX_Callback(app, event)
            % Create GUIDE-style callback args - Added by Migration Tool
            [hObject, eventdata, handles] = convertToGUIDECallbackArguments(app, event); %#ok<ASGLU>
            
                % hObject    handle to abrirFileRX (see GCBO)
                % eventdata  reserved - to be defined in a future version of MATLAB
                % handles    structure with handles and user data (see GUIDATA)
                ext = extractAfter(handles.file, '.');
                fName = extractBefore(handles.file, '.');
                filename = strcat(handles.path, fName, '_RX.', ext);
                set_file_data(filename, handles.rxdata);
                winopen(filename);
        end

        % Button pushed function: cifraCriptograma
        function cifraCriptograma_Callback(app, event)
            % Create GUIDE-style callback args - Added by Migration Tool
            [hObject, eventdata, handles] = convertToGUIDECallbackArguments(app, event); %#ok<ASGLU>
            
                % hObject    handle to cifraCriptograma (see GCBO)
                % eventdata  reserved - to be defined in a future version of MATLAB
                % handles    structure with handles and user data (see GUIDATA)
                filename = extractBefore(handles.file, '.');
                winopen([handles.path, filename, '.key']);
        end

        % Button pushed function: cifraTextoClaro
        function cifraTextoClaro_Callback(app, event)
            % Create GUIDE-style callback args - Added by Migration Tool
            [hObject, eventdata, handles] = convertToGUIDECallbackArguments(app, event); %#ok<ASGLU>
            
                % hObject    handle to cifraTextoClaro (see GCBO)
                % eventdata  reserved - to be defined in a future version of MATLAB
                % handles    structure with handles and user data (see GUIDATA)
                fname = extractBefore(handles.file, '.');
                ext = extractAfter(handles.file, '.');
                winopen(strcat(handles.path, fname, '_ciphered.', ext));
        end

        % Button pushed function: cifraTextoDecifrado
        function cifraTextoDecifrado_Callback(app, event)
            % Create GUIDE-style callback args - Added by Migration Tool
            [hObject, eventdata, handles] = convertToGUIDECallbackArguments(app, event); %#ok<ASGLU>
            
                % hObject    handle to cifraTextoDecifrado (see GCBO)
                % eventdata  reserved - to be defined in a future version of MATLAB
                % handles    structure with handles and user data (see GUIDATA)
                fname = extractBefore(handles.file, '.');
                ext = extractAfter(handles.file, '.');
                winopen(strcat(handles.path, fname, '_deciphered.', ext));
        end

        % Button pushed function: fileHistogram
        function fileHistogram_Callback(app, event)
            % Create GUIDE-style callback args - Added by Migration Tool
            [hObject, eventdata, handles] = convertToGUIDECallbackArguments(app, event); %#ok<ASGLU>
            
                % hObject    handle to fileHistogram (see GCBO)
                % eventdata  reserved - to be defined in a future version of MATLAB
                % handles    structure with handles and user data (see GUIDATA)
                plot_histogram(handles.data, handles.file);
        end

        % Button pushed function: openFileTX
        function openFileTX_Callback(app, event)
            % Create GUIDE-style callback args - Added by Migration Tool
            [hObject, eventdata, handles] = convertToGUIDECallbackArguments(app, event); %#ok<ASGLU>
            
                % hObject    handle to openFileTX (see GCBO)
                % eventdata  reserved - to be defined in a future version of MATLAB
                % handles    structure with handles and user data (see GUIDATA)
            
                [filename, pathname] = uigetfile;
                handles.file =  filename; % Add filename to handles
                handles.path = pathname;  % Add pathname to handles
            
                set(handles.fileHistogram,'Enable','On');
                set(handles.simSCD,'Enable','On');
            
                pathed_filename = [handles.path, handles.file];
                handles.data = get_file_data(pathed_filename); % Add data to handles
            
                % get stats from data --> [symbol ocurrences probabilities]
                handles.symbol_stats = get_symbol_stats(handles.data);
                guidata(hObject,handles); % Update handles structure
            
                % calculate entropy
                bits = get_num_bits(handles.data);
                H = entropy(handles.symbol_stats);
            
                % update handles
                app.Label.Position(3) = app.Label.Position(3)*2;
                set(handles.entropiaVal,'string', round(H, 4));
                app.Label_2.Position(3) = app.Label_2.Position(3)*2;
                set(handles.text18, 'string', num2str(bits));
        end

        % Button pushed function: sairBtn
        function sairBtn_Callback(app, event)
            % Create GUIDE-style callback args - Added by Migration Tool
            [hObject, eventdata, handles] = convertToGUIDECallbackArguments(app, event); %#ok<ASGLU>
            
                % hObject    handle to sairBtn (see GCBO)
                % eventdata  reserved - to be defined in a future version of MATLAB
                % handles    structure with handles and user data (see GUIDATA)
                close all
            
                % remove files created in coding / decoding operations
                delete([handles.path, '*.key']);
                delete([handles.path, '*_deciphered*']);
                delete([handles.path, '*_ciphered*']);
                delete([handles.path, '*_RX*']);
                delete([handles.path, '*.zip']);
        end

        % Button pushed function: simSCD
        function simSCD_Callback(app, event)
            % Create GUIDE-style callback args - Added by Migration Tool
            [hObject, eventdata, handles] = convertToGUIDECallbackArguments(app, event); %#ok<ASGLU>
            
                % hObject    handle to simSCD (see GCBO)
                % eventdata  reserved - to be defined in a future version of MATLAB
                % handles    structure with handles and user data (see GUIDATA)
                set(handles.abrirFileRX,'Enable','On');
            
                % desable cifra buttons if semCifra is selected
                if get(handles.semCifra, 'Value')
                    set(handles.cifraTextoClaro,'Enable','Off');
                    set(handles.cifraCriptograma,'Enable','Off');
                    set(handles.cifraTextoDecifrado,'Enable','Off');
                else
                    set(handles.cifraTextoClaro,'Enable','On');
                    set(handles.cifraCriptograma,'Enable','On');
                    set(handles.cifraTextoDecifrado,'Enable','On');
                end
            
                % encode data
                [encoded_data, code] = encodeTX(app, hObject, eventdata, handles);
            
                % set random erros in encoded data acording to BER
                encoded_data = set_errors(app, hObject, eventdata, handles, encoded_data);
            
                % decode data
                decoded_data = decodeRX(app, hObject, eventdata, handles, encoded_data, code);
            
                % set RX data
                handles.rxdata = set_bits(decoded_data);
            
                % Update handles structure
                guidata(hObject, handles);
        end
    end

    
    % Component initialization
    methods (Access = private)

        % Create UIFigure and components
        function createComponents(app)

            % Create SCDSimuladorUIFigure and hide until all components are created
            app.SCDSimuladorUIFigure = uifigure('Visible', 'off');
            app.SCDSimuladorUIFigure.Position = [100 100 1118 497];
            app.SCDSimuladorUIFigure.Name = 'SCD - Simulador';
            app.SCDSimuladorUIFigure.Resize = 'off';
            app.SCDSimuladorUIFigure.HandleVisibility = 'callback';
            app.SCDSimuladorUIFigure.Tag = 'SCDSimulatorUIFigure';

            % Create Image
            app.Image = uiimage(app.SCDSimuladorUIFigure);
            app.Image.Position = [18 387 100 78];
            app.Image.ImageSource = 'digital-communication-866253.png';

            % Create SCDSimuladorGrupo01TurmaLI31NLabel
            app.SCDSimuladorGrupo01TurmaLI31NLabel = uilabel(app.SCDSimuladorUIFigure);
            app.SCDSimuladorGrupo01TurmaLI31NLabel.HorizontalAlignment = 'center';
            app.SCDSimuladorGrupo01TurmaLI31NLabel.FontSize = 60;
            app.SCDSimuladorGrupo01TurmaLI31NLabel.FontWeight = 'bold';
            app.SCDSimuladorGrupo01TurmaLI31NLabel.Position = [276 398 570 86];
            app.SCDSimuladorGrupo01TurmaLI31NLabel.Text = 'SCD - Simulador';
            
            % Create ComunicaesTurmaLI31NGrupo01Label
            app.ComunicaesTurmaLI31NGrupo01Label = uilabel(app.SCDSimuladorUIFigure);
            app.ComunicaesTurmaLI31NGrupo01Label.HorizontalAlignment = 'center';
            app.ComunicaesTurmaLI31NGrupo01Label.FontSize = 12;
            app.ComunicaesTurmaLI31NGrupo01Label.Position = [863 410 254 22];
            app.ComunicaesTurmaLI31NGrupo01Label.Text = 'Comunica��es - Turma LI31N - Grupo01';

            % Create FicheiroatransmitirPanel
            app.FicheiroatransmitirPanel = uipanel(app.SCDSimuladorUIFigure);
            app.FicheiroatransmitirPanel.TitlePosition = 'centertop';
            app.FicheiroatransmitirPanel.Title = 'Ficheiro a transmitir';
            app.FicheiroatransmitirPanel.FontWeight = 'bold';
            app.FicheiroatransmitirPanel.Position = [19 179 206 182];

            % Create AbrirButton
            app.AbrirButton = uibutton(app.FicheiroatransmitirPanel, 'push');
            app.AbrirButton.ButtonPushedFcn = createCallbackFcn(app, @openFileTX_Callback, true);
            app.AbrirButton.Tag = 'openFileTX';
            app.AbrirButton.Position = [53 122 100 22];
            app.AbrirButton.Text = 'Abrir';

            % Create HistogramaButton
            app.HistogramaButton = uibutton(app.FicheiroatransmitirPanel, 'push');
            app.HistogramaButton.ButtonPushedFcn = createCallbackFcn(app, @fileHistogram_Callback, true);
            app.HistogramaButton.Tag = 'fileHistogram';
            app.HistogramaButton.Position = [53 91 100 22];
            app.HistogramaButton.Text = 'Histograma';

            % Create EntropiaLabel
            app.EntropiaLabel = uilabel(app.FicheiroatransmitirPanel);
            app.EntropiaLabel.Position = [10 60 90 22];
            app.EntropiaLabel.Text = 'Entropia - H(x) ';

            % Create Label
            app.Label = uilabel(app.FicheiroatransmitirPanel);
            app.Label.Tag = 'entropiaVal';
            app.Label.Position = [93 60 48 22];
            app.Label.Text = '';

            % Create bitsmboloLabel
            app.bitsmboloLabel = uilabel(app.FicheiroatransmitirPanel);
            app.bitsmboloLabel.Position = [130 60 70 22];
            app.bitsmboloLabel.Text = 'bit / s�mbolo';

            % Create bitsLabel
            app.bitsLabel = uilabel(app.FicheiroatransmitirPanel);
            app.bitsLabel.Position = [43 10 42 22];
            app.bitsLabel.Text = '#bits - ';

            % Create Label_2
            app.Label_2 = uilabel(app.FicheiroatransmitirPanel);
            app.Label_2.Tag = 'text18';
            app.Label_2.Position = [84 10 100 22];
            app.Label_2.Text = '';

            % Create UIAxes
            app.UIAxes = uiaxes(app.SCDSimuladorUIFigure);
            title(app.UIAxes, 'Histograma')
            xlabel(app.UIAxes, 'X')
            ylabel(app.UIAxes, 'Y')
            app.UIAxes.Tag = 'histogramAxes';
            app.UIAxes.Position = [244 179 627 209];

            % Create BEREditFieldLabel
            app.BEREditFieldLabel = uilabel(app.SCDSimuladorUIFigure);
            app.BEREditFieldLabel.HorizontalAlignment = 'right';
            app.BEREditFieldLabel.Position = [43 58 44 22];
            app.BEREditFieldLabel.Text = 'BER = ';

            % Create BEREditField
            app.BEREditField = uieditfield(app.SCDSimuladorUIFigure, 'text');
            app.BEREditField.Tag = 'berVal';
            app.BEREditField.Position = [102 58 100 22];
            app.BEREditField.Value = '10e-5';

            % Create CodificaodefonteButtonGroup
            app.CodificaodefonteButtonGroup = uibuttongroup(app.SCDSimuladorUIFigure);
            app.CodificaodefonteButtonGroup.TitlePosition = 'centertop';
            app.CodificaodefonteButtonGroup.Title = 'Codifica��o de fonte';
            app.CodificaodefonteButtonGroup.FontWeight = 'bold';
            app.CodificaodefonteButtonGroup.Position = [244 47 168 106];

            % Create AusnciadecodificaoButton
            app.AusnciadecodificaoButton = uitogglebutton(app.CodificaodefonteButtonGroup);
            app.AusnciadecodificaoButton.Tag = 'codfonteAusencia';
            app.AusnciadecodificaoButton.Text = 'Aus�ncia de codifica��o';
            app.AusnciadecodificaoButton.Position = [11 53 145 22];
            app.AusnciadecodificaoButton.Value = true;

            % Create CdigodeHuffmanButton
            app.CdigodeHuffmanButton = uitogglebutton(app.CodificaodefonteButtonGroup);
            app.CdigodeHuffmanButton.Tag = 'codfonteHuffman';
            app.CdigodeHuffmanButton.Text = 'C�digo de Huffman';
            app.CdigodeHuffmanButton.Position = [11 32 145 22];

            % Create FormatoZIPButton
            app.FormatoZIPButton = uitogglebutton(app.CodificaodefonteButtonGroup);
            app.FormatoZIPButton.Tag = 'codfonteZIP';
            app.FormatoZIPButton.Text = 'Formato ZIP';
            app.FormatoZIPButton.Position = [11 11 145 22];

            % Create CifraButtonGroup
            app.CifraButtonGroup = uibuttongroup(app.SCDSimuladorUIFigure);
            app.CifraButtonGroup.TitlePosition = 'centertop';
            app.CifraButtonGroup.Title = 'Cifra';
            app.CifraButtonGroup.FontWeight = 'bold';
            app.CifraButtonGroup.Position = [427 47 267 105];

            % Create SemcifraButton
            app.SemcifraButton = uitogglebutton(app.CifraButtonGroup);
            app.SemcifraButton.Tag = 'semCifra';
            app.SemcifraButton.Text = 'Sem cifra';
            app.SemcifraButton.Position = [62 54 145 22];

            % Create OnetimePadVernamButton
            app.OnetimePadVernamButton = uitogglebutton(app.CifraButtonGroup);
            app.OnetimePadVernamButton.Tag = 'cifraVern';
            app.OnetimePadVernamButton.Text = 'One-time Pad Vernam';
            app.OnetimePadVernamButton.Position = [62 32 145 22];

            % Create TextoemclaroButton
            app.TextoemclaroButton = uibutton(app.CifraButtonGroup, 'push');
            app.TextoemclaroButton.ButtonPushedFcn = createCallbackFcn(app, @cifraTextoClaro_Callback, true);
            app.TextoemclaroButton.Tag = 'cifraTextoClaro';
            app.TextoemclaroButton.Enable = 'off';
            app.TextoemclaroButton.Position = [1 1 87 22];
            app.TextoemclaroButton.Text = 'Texto em claro';

            % Create CriptogramaButton
            app.CriptogramaButton = uibutton(app.CifraButtonGroup, 'push');
            app.CriptogramaButton.ButtonPushedFcn = createCallbackFcn(app, @cifraCriptograma_Callback, true);
            app.CriptogramaButton.Tag = 'cifraCriptograma';
            app.CriptogramaButton.Enable = 'off';
            app.CriptogramaButton.Position = [86 1 87 22];
            app.CriptogramaButton.Text = 'Criptograma';

            % Create TextodecifradoButton
            app.TextodecifradoButton = uibutton(app.CifraButtonGroup, 'push');
            app.TextodecifradoButton.ButtonPushedFcn = createCallbackFcn(app, @cifraTextoDecifrado_Callback, true);
            app.TextodecifradoButton.Tag = 'cifraTextoDecifrado';
            app.TextodecifradoButton.Enable = 'off';
            app.TextodecifradoButton.Position = [172 1 95 22];
            app.TextodecifradoButton.Text = 'Texto decifrado';

            % Create CodificaodecanalButtonGroup
            app.CodificaodecanalButtonGroup = uibuttongroup(app.SCDSimuladorUIFigure);
            app.CodificaodecanalButtonGroup.TitlePosition = 'centertop';
            app.CodificaodecanalButtonGroup.Title = 'Codifica��o de canal';
            app.CodificaodecanalButtonGroup.FontWeight = 'bold';
            app.CodificaodecanalButtonGroup.Position = [703 47 168 106];

            % Create SemcontrolodeerrosButton
            app.SemcontrolodeerrosButton = uitogglebutton(app.CodificaodecanalButtonGroup);
            app.SemcontrolodeerrosButton.Tag = 'codcanalAusencia';
            app.SemcontrolodeerrosButton.Text = 'Sem controlo de erros';
            app.SemcontrolodeerrosButton.Position = [11 53 145 22];

            % Create Bitparidadepar87Button
            app.Bitparidadepar87Button = uitogglebutton(app.CodificaodecanalButtonGroup);
            app.Bitparidadepar87Button.Tag = 'codcanalBitPPar';
            app.Bitparidadepar87Button.Text = 'Bit paridade par (8,7)';
            app.Bitparidadepar87Button.Position = [11 32 145 22];

            % Create CdigoHamming74Button
            app.CdigoHamming74Button = uitogglebutton(app.CodificaodecanalButtonGroup);
            app.CdigoHamming74Button.Tag = 'codcanalH74';
            app.CdigoHamming74Button.Text = 'C�digo Hamming (7,4)';
            app.CdigoHamming74Button.Position = [11 11 145 22];

            % Create FicheirorecebidoPanel
            app.FicheirorecebidoPanel = uipanel(app.SCDSimuladorUIFigure);
            app.FicheirorecebidoPanel.TitlePosition = 'centertop';
            app.FicheirorecebidoPanel.Title = 'Ficheiro recebido';
            app.FicheirorecebidoPanel.FontWeight = 'bold';
            app.FicheirorecebidoPanel.Position = [895 179 206 182];

            % Create AbrirButton_2
            app.AbrirButton_2 = uibutton(app.FicheirorecebidoPanel, 'push');
            app.AbrirButton_2.ButtonPushedFcn = createCallbackFcn(app, @abrirFileRX_Callback, true);
            app.AbrirButton_2.Tag = 'abrirFileRX';
            app.AbrirButton_2.Position = [53 122 100 22];
            app.AbrirButton_2.Text = 'Abrir';

            % Create bitsLabel_2
            app.bitsLabel_2 = uilabel(app.SCDSimuladorUIFigure);
            app.bitsLabel_2.Position = [255 12 42 22];
            app.bitsLabel_2.Text = '#bits - ';

            % Create Label_3
            app.Label_3 = uilabel(app.SCDSimuladorUIFigure);
            app.Label_3.Tag = 'text17';
            app.Label_3.Position = [296 12 100 22];
            app.Label_3.Text = '';

            % Create bitsLabel_3
            app.bitsLabel_3 = uilabel(app.SCDSimuladorUIFigure);
            app.bitsLabel_3.Position = [714 12 42 22];
            app.bitsLabel_3.Text = '#bits - ';

            % Create Label_4
            app.Label_4 = uilabel(app.SCDSimuladorUIFigure);
            app.Label_4.Tag = 'text19';
            app.Label_4.Position = [755 12 100 22];
            app.Label_4.Text = '';

            % Create BERdatransmissoLabel
            app.BERdatransmissoLabel = uilabel(app.SCDSimuladorUIFigure);
            app.BERdatransmissoLabel.Position = [895 58 129 22];
            app.BERdatransmissoLabel.Text = 'BER da transmiss�o = ';

            % Create Label_5
            app.Label_5 = uilabel(app.SCDSimuladorUIFigure);
            app.Label_5.Tag = 'text16';
            app.Label_5.Position = [1023 58 100 22];
            app.Label_5.Text = '';

            % Create SimularButton
            app.SimularButton = uibutton(app.SCDSimuladorUIFigure, 'push');
            app.SimularButton.ButtonPushedFcn = createCallbackFcn(app, @simSCD_Callback, true);
            app.SimularButton.Tag = 'simSCD';
            app.SimularButton.FontSize = 24;
            app.SimularButton.FontWeight = 'bold';
            app.SimularButton.Position = [895 91 206 43];
            app.SimularButton.Text = 'Simular';

            % Create SairButton
            app.SairButton = uibutton(app.SCDSimuladorUIFigure, 'push');
            app.SairButton.ButtonPushedFcn = createCallbackFcn(app, @sairBtn_Callback, true);
            app.SairButton.Tag = 'sairBtn';
            app.SairButton.Position = [1001 12 100 22];
            app.SairButton.Text = 'Sair';

            % Show the figure after all components are created
            app.SCDSimuladorUIFigure.Visible = 'on';
        end
    end

    % App creation and deletion
    methods (Access = public)

        % Construct app
        function app = SCD_Simulator_app

            % Create UIFigure and components
            createComponents(app)

            % Register the app with App Designer
            registerApp(app, app.SCDSimuladorUIFigure)

            if nargout == 0
                clear app
            end
        end

        % Code that executes before app deletion
        function delete(app)

            % Delete UIFigure when app is deleted
            delete(app.SCDSimuladorUIFigure)
        end
    end
end