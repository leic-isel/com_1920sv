%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% get_file_data.m
%  
%

function data = get_file_data( filename )
    % GET_FILE_DATA 
    % Receives a filename to extract the binary data with 'unchar' precision. 
    
    % Open file with permission 'rb' (r read, b Big-endian ordering)
    fileID = fopen( filename, 'rb' );

    % Check descriptor to see if the file has open without errors.
    if -1==fileID
        error('Error opening file %s for reading',filename);
    end

    % Read entire file to a vector.
    %
    % fileID - File identifier
    % Inf - Column vector, with each element containing a value of the file
    % 'unchar' - precision (class and size of values to read), unchar is 8 bits
    data = fread( fileID, Inf, 'uchar' );
    
    data = data'; % Transpose to a row vector.
    fclose( fileID ); % Close file  
    
end