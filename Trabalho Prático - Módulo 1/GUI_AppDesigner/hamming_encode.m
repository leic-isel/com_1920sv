%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% hamming_encode.m
%  
%

function encoded = hamming_encode( bitvector )
    % HAMMING_ENCODE
    % Receives a bit vector from a file
    % Returns the bit vector hamming encoded (7,4)
    
    len = length(bitvector);
    
    % builld encoded vector
    encoded = [];
    for i=1 : 4 : len       
        encoded = [encoded, encode(bitvector(i:i+3))];
    end
end

function n = encode(k)
    % ENCODE 
    % recives k, a vector of zeros and ones of the message
    % returns n, a vector of zeros and ones of the code word to transmit
    % after xor operate the bits in k

    % calculate parity bits
    b0 = xor(xor(k(2), k(3)), k(4));
    b1 = xor(xor(k(1), k(2)), k(4));
    b2 = xor(xor(k(1), k(3)), k(4));

    % append parity bits to the mensage    
    n = [k, b0, b1, b2];
end

