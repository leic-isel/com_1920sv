%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunica��es. 
%
% Trabalho Pr�tico - M�dulo 1
% 32398 H�lder Augusto
% 39619 F�bio Teixeira
% 45824 Nuno Ven�ncio
%
% huffman.m
%  
%

function [nBits, code, bitdata] = huffman( filedata, symbol_stats )
    % HUFFMAN
    % Receives a matrix with symbols, symbol_ocurrences and symbol probabilities
    % Returns the data size after encoding, in bits   

    symb = symbol_stats(:,1); % symbols list
    prob = symbol_stats(:,3); % symbol probabilities
    
    % Get huffman code and his average length
    code = huffmandict(symb,prob);
    
    % set filedata in 8bit data
    data = set_bits(filedata);

    % encode data with huffman code
    bitdata = huffmanenco(data', code);
    bitdata = bitdata';
    
    % Calculate data size after encoding
    nBits = length(bitdata);
    
end