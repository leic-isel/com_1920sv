%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% huffman.m
%  
%

function [data, zipBits] = zipfile( file )
    % ZIPFILE
    % Receives a filename to zip
    % Returns the 8bit data and the number of bits in the file after ziping    
    
    zip('zipfile', file);                  % zip the file    
    data = get_file_data( 'zipfile.zip' ); % get data from zipfile
    zipBits = get_num_bits( data );        % get number of bits    
end