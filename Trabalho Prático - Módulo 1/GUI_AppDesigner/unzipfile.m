%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% unzipfile.m
%  
%
function unzipBits = unzipfile()
    % ZIPFILE
    % Returns the bit data in the file after unziping

    filename = unzip('zipfile.zip');     % unzip the file
    data = get_file_data( filename{1} ); % get data from zipfile
    unzipBits = get_bits(data);			 % get binary data
end