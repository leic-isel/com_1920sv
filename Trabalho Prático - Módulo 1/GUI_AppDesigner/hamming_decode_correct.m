%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% hamming_decode_correct.m
%  
%

function decoded = hamming_decode_correct( bitstream )
    % HAMMING_DECODE_CORRECT
    % Receives a bistream of data encoded in Hamming (7,4)
    % Computes de hamming decoding and correts errors
    % Returns the bit vector decoded
    
    decoded = [];
    for i=1 : 7 : length(bitstream)
        decoded = [decoded, decode(bitstream(i:i+6))];
    end
end

function k = decode(n)
    % DECODE
    % Receives the a code word in bit vector
    % Returns the message part of the code word decoded and corrected in
    % case of 1 error
    
    % look at the message part of the code word and recalculate the parity
    % bits
    expected_parity_bits = recalculate_parity(n(1:4));
    
    % get sindrome
    sindrome = get_sindrome(expected_parity_bits, [n(5), n(6), n(7)]);
    
    % get error pattern
    error_pattern = get_error_pattern(sindrome);
    
    % correct errors in the code word (only corrects 1 error)
    corrected = correct_errors(n, error_pattern);
    
    % get the message part of the code word corrected
    k = corrected(1:4);
end

function p_bits = recalculate_parity(n)
    % RECALCULATE_PARITY
    % Receives the message part of the code word
    % Return the XOR calculation of the 3 parity bits
    
    b0 = xor(xor(n(2), n(3)), n(4));
    b1 = xor(xor(n(1), n(2)), n(4));
    b2 = xor(xor(n(1), n(3)), n(4));
    
    p_bits = [b0, b1, b2];
end

function sindrome = get_sindrome(expected_parity_bits, parity_bits)
    % GET_SINDROME
    % Receives the parity bits calculated from the message part of the code
    % word received and the parity bits received in the code word
    % Return the XOR operation of the 2 parts
    
    s1 = xor(expected_parity_bits(1), parity_bits(1));
    s2 = xor(expected_parity_bits(2), parity_bits(2));
    s3 = xor(expected_parity_bits(3), parity_bits(3));
    
    sindrome = [s1, s2, s3];
end

function error_pattern = get_error_pattern(sindrome)
    % GET_ERROR_PATTERN
    % Receives a sindrome
    % Returns an error pattern
    
    error_pattern = zeros(1, 7);
    
    if sindrome == [0, 1, 1]
        error_pattern(1) = 1;
    elseif sindrome == [1, 1, 0]
        error_pattern(2) = 1;
    elseif sindrome == [1, 0, 1]
        error_pattern(3) = 1;
    elseif sindrome == [1, 1, 1]
        error_pattern(4) = 1;
    end
end

function corrected = correct_errors(n, error_pattern)
    % CORRECT_ERRORS
    % Receives a code word and an error_pattern
    % Returns the code word corrected
    
    corrected = zeros(1, 7);
    for i=1 : 7
        corrected(i) = xor(n(i), error_pattern(i));
    end
end