%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% get_num_bits.m
%  
%

function num_bits = get_num_bits( data )
    % GET_NUM_BITS
    % Receives data with 'uchar' precision from a file
    % Returns the number of bits in that data

    num_bits = length(data) * 8; % multiplied by 8 because unchar precision

end 