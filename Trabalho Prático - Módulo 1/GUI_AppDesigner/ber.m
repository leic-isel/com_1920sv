%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% ber.m
%  
%

function [bits_with_error, total_errors] = ber (file_bits, ber_value)
	% BER
    % Receives a bit vector of data from a file and a BER value
    % Returns the same bit vector with errors introduced and the total number of
    % errors introduced

    % vector of length equal to the length of the bit vector from a file 
    % composed of zeros and ones. The ones have probability less than the BER
    % value
    a = rand(1,length(file_bits)) < ber_value;
    a = double(a);
    
    % introduce errors in the original data by XOR operate that data with the
    % generated vector
    bits_with_error = xor(file_bits,a);
    bits_with_error = double(bits_with_error);

    % count the total number of errors introduced in the original data
    total_errors = abs(sum(bits_with_error)-sum(file_bits));   
    
end