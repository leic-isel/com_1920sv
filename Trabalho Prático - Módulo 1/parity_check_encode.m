%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% parity_check_encode.m
%  
%

function encoded = parity_check_encode( filename, k )
    % PARITY_CHECK_ENCODE
    % Receives a filename and the number of message bits (k)
    % Returns the bit vector encoded
    
    % n is the number of message bits
    bitvector = get_file_bits(filename);
    len = length(bitvector);
    
    % calculate the number of bits to append to the last message word
    num_bits_to_append = k - rem(len, k);
    
    % append bits with value 0 to the last message word
    bitvector = [bitvector, zeros(1, num_bits_to_append)];
    len = length(bitvector); % recalculate the new length    
    
    % builld encoded vector    
    %encoded = zeros(1, len + ceil(len/k));
    encoded = [];
    for i=1 : k : len       
        encoded = [encoded, encode(bitvector(i:i+k-1))];
    end     
end

function n = encode(k)
    % ENCODE 
    % recives k, a vector of zeros and ones of the message
    % returns n, a vector of zeros and ones of the code word to transmit
    % after xor operate the bits in k

    for i=1 : length(k)-1        
        if i==1 % first iteraction, XOR operate 1st and 2nd bits
            xor_op = xor(k(i), k(i+1));        
        else % next interactions, XOR operate previous result with next bit
            xor_op = xor(xor_op, k(i+1));
        end
    end

    % append the result of XOR operations to the mensage    
    n = [k,xor_op];
end

function bitvector = get_file_bits( filename )
    % GET_FILE_BITS
    % Receives a filename
    % Returns a bit vector containing the file data

    fileID = fopen( filename, 'rb' ); % open file for read
    
    if -1==fileID % Check descriptor.
        error('Error opening file %s for reading',filename);
    end

    data = fread( fileID, Inf, 'uchar' ); % read file to a vector
    
    fclose( fileID ); % close file
    
    data = data'; % Transpose to a row vector
    
    % get file bits
    k = 1;
    for i=1 : length(data)
        bitvector(k:k+7) = bitget( data(i), 8:-1:1 );
        k = k + 8;
    end
end


