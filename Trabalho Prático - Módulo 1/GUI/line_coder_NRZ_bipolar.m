%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Pr�tico - Módulo 2
% 32398 Helder Augusto 
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% line_coder_NRZ_bipolar.m
%  
%

function output = line_coder_NRZ_bipolar( bits, amplitude, samples )
    % LINE CODER NRZ BIPOLAR
    % Receives data in bits, voltage amplitude and number of samples
    % Returns the bits times samples with voltage equal to amplitude if 
    % the original bit was 1 or voltage equal to negative amplitude
    % otherwise
     
    for i=1 : length(bits)
        %Determine if bit at position i is 1 or 0 
        if bits(i) 
            % if samples = 5, amplitude = 3, bits = [1 1 0]
            % i=1 -> output(1*5-5+1 = 1 : 1*5 = 5)  = [3 3 3 3 3 ]
            % i=2 -> output(2*5-5+1 = 6 : 2*5 = 10) = [3 3 3 3 3 | 3 3 3 3 3]
            output(i*samples-samples+1 : i*samples) = ones(1, samples)*amplitude;            
        else
            % i=3 -> output(3*5-5+1 = 11 : 4*5 = 15 = [3 3 3 3 3 | 3 3 3 3
            % 3 | -3 -3 -3 -3 -3]
            output(i*samples-samples+1 : i*samples) = ones(1, samples)*-amplitude;
        end
    end
end