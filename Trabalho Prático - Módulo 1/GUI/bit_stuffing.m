%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% bit_stuffing.m
%  
%

function out_bits = bit_stuffing( in_bits )
	% BIT STUFFING
    % Receives data in bits from a file
    % Returns the same data with bit stuffing
    

    out_bits = []; 
    counter = 1;
    stuffed = false;
    
    out_bits = [out_bits in_bits(1)];
    for i=2 : length(in_bits)
       if in_bits(i) == in_bits(i-1) && ~stuffed
           counter = counter + 1;
           if counter == 6
               tmp = in_bits(i) == 0 : 1 : 0; %Use of ternary operator
               out_bits = [out_bits in_bits(i) tmp];
               counter = 1;
               stuffed = true;
           else
               out_bits = [out_bits in_bits(i)];
           end
       else
           counter = 1;
           out_bits = [out_bits in_bits(i)];
           stuffed = false;
       end           
    end
end