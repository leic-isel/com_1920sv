%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% plot_histogram.m
%  
%

function plot_histogram( data, filename )
    % PLOT_HISTOGRAM 
    % Receives data with 'unchar' precision from a file and his pathname
    % Plots the corresponding data, counting symbol occurrences
    cla reset;
    hist(data, 0:1:255);
    grid on;
    xlabel('Símbolos');
    ylabel('Ocorrências');    
    title(filename, 'FontSize', 10);
    
end 