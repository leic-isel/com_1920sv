%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% get_bits.m
%  
%

function bits = get_bits( file_data )
	% GET_BITS
    % Receives a vector of data from a file in 8bit integers
    % Returns a vector with the same data in bits
    
    k = 1;
    for i=1 : length(file_data)
        bits(k:k+7) = bitget( file_data(i), 8:-1:1 );
        k = k + 8;
    end
end