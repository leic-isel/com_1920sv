%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% generate_cipher_key.m
%  
%

function key = generate_cipher_key( filename, size )
	% GENERATE_CIPHER_KEY
    % Receives the number of bits for the length of the key
    % Returns the key    
    
    filename = extractBefore(filename, '.'); % remove filename extension
    key = randi([0,1], 1, size); % array of random zeros and ones of length size
    key8bit = set_bits(key); % turn k in 8bit integers  
    set_file_data( [filename, '.key'], key8bit ); % save key to a file
end