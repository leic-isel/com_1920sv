%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% one_time_pad_cipher.m
%  
%

function [cipher, num_bits_to_append] = one_time_pad_cipher( file_data, filename )
	% ONE_TIME_PAD_CIPHER
    % Receives data in bits from a file and the corresponding filename
    % Returns the name of the file ciphered and the data from that file

    % calculate the number of bits to append to file_data in case it's not
    % multiple of 8
    num_bits_to_append = 0;
    if rem(length(file_data), 8) ~= 0
    	num_bits_to_append = 8 - rem(length(file_data), 8);
    end
    
    % append trailing bits
    if num_bits_to_append > 0
        file_data = [file_data, zeros(1, num_bits_to_append)];
    end

    % get the key to cipher
    k = generate_cipher_key( filename, length(file_data) );

    % XOR operate the data_bits with the key
    cipher = bitxor(file_data, k);

    % turn cipher in 8 bit integers
    ciphered = set_bits(cipher);
    
    % save ciphered data to a file
    ext = extractAfter (filename, '.'); % get extension
    fName = extractBefore(filename, '.'); % remove extension
    ciphfile = [fName, '_ciphered.', ext];
    set_file_data( ciphfile, ciphered );
end

