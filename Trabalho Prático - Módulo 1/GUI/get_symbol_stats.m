%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% get_symbol_stats.m
%  
%

function symbol_stats = get_symbol_stats( data )
    % GET_SYMBOL_STATS
    % Receives data with 'unchar' precision from a file 
    % Returns a matrix with symbols, symbol_ocurrences and symbol_probabilities

    % Find the unique values of A and the index vectors ~ and iSymbols, 
    % such that Symbols = A(ia) and A = Symbols(iSymbols). 
    [Symbols, ~, iSymbols] = unique(data);

    % A = accumarray(subs,val) returns array A by accumulating elements of 
    % vector val using the subscripts subs. If subs is a column vector, then 
    % each element defines a corresponding subscript in the output, which is 
    % also a column vector. The accumarray function collects all elements of 
    % val that have identical subscripts in subs and stores their sum in the 
    % location of A corresponding to that subscript.
    occurrences = accumarray(iSymbols, 1);

    % Create new matrix with transpose Simbols, symbol_occurrences and
    % probabilities
    symbol_stats = [Symbols', occurrences, occurrences / length(data)];

end