function [ output ] = ook_modulation_decoder( bits, amplitude, frequency, samples)
    %On-Off Keying modulation decoder
    % *** Input paremeters ***
    % message - modulated message received
    % amplitude - amplitude used to modulate bit 1
    % frequency - frequency used to modulate the bits
    % samples - the given number of samples

    %Ceate the output array 
    output = zeros(1, length(bits)/samples);

    %Create an array with the size of length(bits) * samples for 
    %the time calculation
    t = 0 : (1/samples) : 1-(1/samples);


    %Create reference carrier wave signal with amplitude
    carrier_wave = cos(2*pi*frequency*t) * amplitude;

    %Calculate bit enery 
    eb = (amplitude^2/2)*samples;

    index = 1;

    for i = 1 : samples : length(bits)

        output(index) = (sum(bits(i: i+samples-1) .* carrier_wave));

        %Decision rules
        if(output(index) > (eb/2)/samples)
            output(index) = 1;    
        else
            output(index) = 0;
        end

        index = index +1;  
    end
end

