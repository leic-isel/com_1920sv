%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% set_file_data.m
%  
%

function set_file_data( filename, file_data )
    % SET_FILE_DATA 
    % Receives a filename to save the file_data to a file and the file_data

    % Open file in write big Endian mode
    fileID = fopen( filename, 'wb');
    
    % Check descriptor
    if -1==fileID
        error( 'Error opening file %s for writing ', filename );
    end
    
    % Write the vector to file
    fwrite( fileID, file_data, 'uchar' );
    
    % Close the file
    fclose( fileID ); 