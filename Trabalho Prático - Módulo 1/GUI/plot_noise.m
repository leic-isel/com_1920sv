function plot_noise(input_bits, output_bits, snr, modulation)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    cla reset;
    our_ber = sum(xor(input_bits, output_bits)) / length(input_bits);
    if (isinf(our_ber))
        our_ber = 0;
    end
    M = 2;
    snr_range = 0:18;
    
    %plot(snr, ber);
    [ber, ser] = berawgn(snr_range, modulation, M, 'coherent');
    semilogy(snr_range, ser);
    hold on;
    semilogy(snr, our_ber, 'r*');
    grid on;
    xlabel('Eb/N0[dB]');
    ylabel('BER');
    title('Curva de BER');
end

