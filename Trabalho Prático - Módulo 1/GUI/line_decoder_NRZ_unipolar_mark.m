% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% line_decoder_NRZ_unipolar_mark.m
%  
%

function output = line_decoder_NRZ_unipolar_mark( bits, amplitude, n_samples )
    % LINE CODER NRZ UNIPOLAR MARK
    % Receives data in bits, voltage amplitude and number of samples
    % Returns the bits times n_samples shifting voltage if 
    % the original bit was 1 or mantaining voltage otherwise
    output = ones(1, length(bits) / n_samples);
    actual_bit = bits(1:n_samples);
    
    j = 1;
    if sum(actual_bit) > (amplitude / 2)
        output(j) = 0;
    else
        output(j) = 1;
    end
    
    
    for i=n_samples+1 : n_samples : length(bits)
        j = j + 1;
        
        if i - length(bits) -1  == n_samples
            break;
        end
        
        next_bit = bits(i : i+n_samples-1);
        
        if abs(sum(actual_bit) - sum(next_bit)) > (amplitude / 2)
            output(j) = 1;
        else
            output(j) = 0;
        end
        
        actual_bit = next_bit;            
    end    
end