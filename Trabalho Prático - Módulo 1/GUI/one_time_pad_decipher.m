%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% one_time_pad_decipher.m
%  
%

function deciph_data = one_time_pad_decipher( file_data, filename, num_bits_to_append )
	% ONE_TIME_PAD_DECIPHER
    % Receives data bits from a file and the corresponding filename
    % Returns the name of the file deciphered
    
    % get file extension and filename without extension 
    ext = extractAfter( filename, '.' );
    fName = extractBefore( filename, '.' );

    % get the key to decipher
    key = get_file_data( [fName, '.key'] );

    % get bits from key
    key_bits = get_bits(key);
    
    % XOR operate data_bits with key_bits
    deciph_data = bitxor(file_data, key_bits);    

    % turn decipher in 8 bit integers
    deciphered = set_bits(deciph_data);

    % save the deciphered file_data in a file
    deciph_name = [fName, '_deciphered.', ext];
    set_file_data( deciph_name, deciphered );
    
    deciph_data = deciph_data(1 : length(deciph_data) - num_bits_to_append);
end