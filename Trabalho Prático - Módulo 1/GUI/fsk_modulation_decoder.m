function [ output ] = fsk_modulation_decoder( msg, amp, fbit1, fbit0, samples )
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here

    %Creates the output array
    output = ones(1, length(msg)/samples);

    %Create an array with the size of length(bits) * samples for 
    %the time calculation. 
    %The step of t, is based on 1 divided by the number of samples.
    t = (0 : 1/samples : 1-(1/samples));

    %Carrier signal for bit = 1
    carrier1 = cos(2*pi*fbit1*t)*amp;
    %Carrier signal for bit = 0
    carrier0 = cos(2*pi*fbit0*t)*amp;

    %Bit energy calculation   %%% //// TODO ///// %%%%
    eb = (amp^2/2)*samples;

    index = 1;

    for i = 1 : samples : length(msg)

        ybit1(index) = sum(msg(i : i+samples-1) .* carrier1);
        ybit0(index) = sum(msg(i : i+samples-1) .* carrier0);

        if (ybit1(index)-ybit0(index)> 0)
            output(index) = 1;
        else
            output(index) = 0;
        end
        index = index + 1; 
    end
end

