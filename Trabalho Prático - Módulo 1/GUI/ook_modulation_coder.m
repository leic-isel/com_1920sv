function [ output ] = ook_modulation_coder( bits, amplitude, frequency, samples )
    %On-Off keying modulation 
    % Parameters: 
    % bits - the input message bits
    % amplitude - the intended aplitude 
    % frequency - the inteded frequency
    % samples - the numbers of samples to be used. 
    % a large number of samples will cause a slow modulation creation.

    %Create the output array 
    output = ones(1, length(bits)*samples);

    %Create an array with the size of length(bits) * samples for 
    %the time calculation. 
    %The step of t, is based on 1 divided by the number of samples.
    t = 0 : (1/samples) : 1 -(1/samples);

    %Create carrier wave signal
    carrier_wave = cos(2*pi*frequency*t);

    %Index of bits array 
    index = 1;

    %Create the modulated message 
    for i = 1 : samples : length(output)-(1/samples) 
        %Create a sequence of bit * samples
        output(i : i+samples-1) = output(i : i+samples-1) .* bits(index);
        %Multiply the sequence by the carrier wave
        output(i : i+samples-1) = output(i : i+samples-1) .* carrier_wave;
        index = index + 1;     
    end

    %Multiply the complete array by the amplitude
    output = output * amplitude;

end