% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% line_coder_NRZ_unipolar_mark.m
%  
%


function output = line_coder_NRZ_unipolar_mark( bits, amplitude, samples )
    % LINE CODER NRZ UNIPOLAR MARK
    % Receives data in bits, voltage amplitude and number of samples
    % Returns the bits times samples shifting voltage if 
    % the original bit was 1 or mantaining voltage otherwise
    
    factor = amplitude;
    for i=1 : length(bits)
        if bits(i) && ~factor
            factor = amplitude;
        elseif bits(i) && factor
            factor = 0;
        end
        output(i*samples-samples+1 : i*samples) = ones(1, samples)*factor;
    end
end