function outbits = channel(inbits, amplitude, samples, alpha, snr, modulation)
    %Receives a bit message and simulates channel noise 
    %by using attenuation and noise
    %Attenuation is passed by parameter
    %Noise is created by using snr parameter and calculate
    %the energy of noise. 

    %Calculate bit energy
    if (modulation == 1)
        eb = (amplitude^2) * samples;
    else
        eb = (amplitude^2/2)*samples;
    end
    %Calculate noise energy
    er = eb / (snr*samples);

    %Generates an array of random noise 
    noise = er * randn(1 , length(inbits));

    %Apply noise and attenuation in message
    outbits = inbits .* alpha + noise;

end

