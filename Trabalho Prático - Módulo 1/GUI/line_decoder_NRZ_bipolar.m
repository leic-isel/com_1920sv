%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% line_decoder_NRZ_bipolar.m
%  
%

function output = line_decoder_NRZ_bipolar( bits, amplitude, n_samples )
    % LINE DECODER NRZ BIPOLAR
    % Receives encoded data in bits, voltage amplitude and number of samples
    % Returns the decoded bits
    known_pulse = ones(1, n_samples) * amplitude;
    output = ones(1, length(bits) / n_samples);
    
    for i=1 : length(output)
        correlator = sum(bits(i*n_samples-n_samples+1 : i*n_samples) .* known_pulse);
        if correlator < 0
            output(i) = 0;
        end
    end
