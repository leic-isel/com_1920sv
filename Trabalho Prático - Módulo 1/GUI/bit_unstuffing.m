%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% bit_unstuffing.m
%  
%

function out_bits = bit_unstuffing( in_bits )
	% BIT UNSTUFFING
    % Receives data in bits from a transmition
    % Returns the same data without the stuffed bits
    
    out_bits = in_bits(1);
    counter = 1;
    discart_bit = false;
    stuffed = false;
    
    for i=2 : length(in_bits)
        if in_bits(i) == in_bits(i-1) && ~stuffed
            counter = counter + 1;
            if counter == 6
                discart_bit = true;
                counter = 1;
                stuffed = true;
            end 
            out_bits = [out_bits in_bits(i)];
        else
            counter = 1;
            if discart_bit
                discart_bit = false;
            else
                out_bits = [out_bits in_bits(i)];
                stuffed = false;
            end
        end        
    end
end