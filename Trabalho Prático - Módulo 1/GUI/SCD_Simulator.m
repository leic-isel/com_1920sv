function varargout = SCD_Simulator(varargin)
    % SCD_SIMULATOR MATLAB code for SCD_Simulator.fig
    %      SCD_SIMULATOR, by itself, creates a new SCD_SIMULATOR or raises the existing
    %      singleton*.
    %
    %      H = SCD_SIMULATOR returns the handle to a new SCD_SIMULATOR or the handle to
    %      the existing singleton*.
    %
    %      SCD_SIMULATOR('CALLBACK',hObject,eventData,handles,...) calls the local
    %      function named CALLBACK in SCD_SIMULATOR.M with the given input arguments.
    %
    %      SCD_SIMULATOR('Property','Value',...) creates a new SCD_SIMULATOR or raises the
    %      existing singleton*.  Starting from the left, property value pairs are
    %      applied to the GUI before SCD_Simulator_OpeningFcn gets called.  An
    %      unrecognized property name or invalid value makes property application
    %      stop.  All inputs are passed to SCD_Simulator_OpeningFcn via varargin.
    %
    %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
    %      instance to run (singleton)".
    %
    % See also: GUIDE, GUIDATA, GUIHANDLES

    % Edit the above text to modify the response to help SCD_Simulator

    % Last Modified by GUIDE v2.5 27-Jun-2020 21:13:42

    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @SCD_Simulator_OpeningFcn, ...
                       'gui_OutputFcn',  @SCD_Simulator_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT

% --- Executes just before SCD_Simulator is made visible. ----------------------
function SCD_Simulator_OpeningFcn(hObject, eventdata, handles, varargin)
    % This function has no output args, see OutputFcn.
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % varargin   command line arguments to SCD_Simulator (see VARARGIN)

    % Choose default command line output for SCD_Simulator
    handles.output = hObject;

    set(handles.codfonteAusencia,'value',1);
    set(handles.codcanalAusencia,'value',1);
    set(handles.noNZR, 'value', 1);

    set(handles.simSCD,'Enable','Off');
    set(handles.fileHistogram,'Enable','Off');
    set(handles.abrirFileRX,'Enable','Off');
    set(handles.cifraTextoClaro,'Enable','Off');
    set(handles.cifraCriptograma,'Enable','Off');
    set(handles.cifraTextoDecifrado,'Enable','Off');
    set(handles.graficar, 'Enable', 'Off');
    set(handles.plotNoise, 'Enable', 'Off');
    
    handles.ciphdata = [];
    handles.rxdata = '';

    % Update handles structure
    guidata(hObject, handles);

    % UIWAIT makes SCD_Simulator wait for user response (see UIRESUME)
    % uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line. -------------
function varargout = SCD_Simulator_OutputFcn(hObject, eventdata, handles) 
    % varargout  cell array for returning output args (see VARARGOUT);
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Get default command line output from handles structure
    varargout{1} = handles.output;

% --- Executes on button press in openFileTX. ----------------------------------
function openFileTX_Callback(hObject, eventdata, handles)
    % hObject    handle to openFileTX (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    [filename, pathname] = uigetfile;
    handles.file =  filename; % Add filename to handles
    handles.path = pathname;  % Add pathname to handles

    set(handles.fileHistogram,'Enable','On');
    set(handles.simSCD,'Enable','On');
    
    pathed_filename = [handles.path, handles.file];
    handles.data = get_file_data(pathed_filename); % Add data to handles   

    % get stats from data --> [symbol ocurrences probabilities]
    handles.symbol_stats = get_symbol_stats(handles.data);
    guidata(hObject,handles); % Update handles structure
    
    % calculate entropy
    bits = get_num_bits(handles.data);
    H = entropy(handles.symbol_stats);
    
    % update handles
    set(handles.entropiaVal,'string', H);
    set(handles.text18, 'string', ['#bits = ', num2str(bits)]);
    
% --- Executes on button press in sairBtn. -------------------------------------
function sairBtn_Callback(hObject, eventdata, handles)
    % hObject    handle to sairBtn (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    close all

    % remove files created in coding / decoding operations
    delete([handles.path, '*.key']);
    delete([handles.path, '*_deciphered*']);
    delete([handles.path, '*_ciphered*']);
    delete([handles.path, '*_RX*']);
    delete([handles.path, '*.zip']);

    delete(handles.figure1);

% --- Executes on button press in simSCD. --------------------------------------
function simSCD_Callback(hObject, eventdata, handles)
    % hObject    handle to simSCD (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    set(handles.abrirFileRX,'Enable','On');    

    % disable cifra buttons if semCifra is selected
    if get(handles.semCifra, 'Value')
        set(handles.cifraTextoClaro,'Enable','Off');
        set(handles.cifraCriptograma,'Enable','Off');
        set(handles.cifraTextoDecifrado,'Enable','Off');
    else
        set(handles.cifraTextoClaro,'Enable','On');
        set(handles.cifraCriptograma,'Enable','On');
        set(handles.cifraTextoDecifrado,'Enable','On');
    end
    
    % disable line coder / modulation plot button if Sem Codificação /
    % Modulação is selected
    if get(handles.noNZR, 'Value')
        set(handles.graficar, 'Enable', 'Off');
    else
        set(handles.graficar, 'Enable', 'On');
    end
    
    % encode data
    [encoded_data, code] = encodeTX(hObject, eventdata, handles);;
    
    % set channel alpha and noise
    if get(handles.nrzBipolar, 'Value')
        modulation = 1;
    elseif get(handles.nrzUnipolarMark, 'Value')
        modulation = 'NRZ-U';
    elseif get(handles.ook, 'Value')
        modulation = 'OOK';
    elseif get(handles.fsk, 'Value')
        modulation = 'FSK';
    else
        modulation = 'None';
    end
    alpha = str2double(get(handles.alfa, 'String'));
    noise_signal = str2double(get(handles.noise, 'String'));
    encoded_data = channel(encoded_data, 5, 10, alpha, noise_signal, modulation);
    
    % allow user to plot BER
    set(handles.plotNoise, 'Enable', 'On');
    
    % decode data
    decoded_data = decodeRX(hObject, eventdata, handles, encoded_data, code);
    
    % show transmission BER
    global input_bits;
    global output_bits;
    
%     if(length(input_bits) < length(output_bits))
%         input_bits = input_bits + zeros(1, length(output_bits)-length(input_bits));
%     elseif(length(input_bits) > length(output_bits))
%         output_bits = output_bits + zeros(1, length(input_bits)-length(output_bits));
%     end  

    ber = sum(xor(input_bits, output_bits)) / length(input_bits);
    set(handles.text16, 'String', ber);
    
    % set RX data
    handles.rxdata = set_bits(decoded_data);
    
    % Update handles structure
    guidata(hObject, handles);
    
% --- Encode TX data -----------------------------------------------------------
function [encoded_data, code] = encodeTX(hObject, eventdata, handles)
    
    encoded_data = get_bits(handles.data);
    global input_bits;
    input_bits = encoded_data;
    code = 0;
    
    % check what radiobutton is selected in Codificação de fonte
    if get(handles.codfonteAusencia, 'Value')
        set(handles.text17, 'string', get(handles.text18, 'String'));
    elseif get(handles.codfonteHuffman, 'Value') 
        [nBits, code, encoded_data] = huffman(encoded_data, handles.symbol_stats);
        set(handles.text17, 'string', ['#bits = ', num2str(nBits)]);
    else
        [zip_data, nbits] = zipfile([handles.path, handles.file]);
        encoded_data = get_bits(zip_data);
        set(handles.text17, 'string', ['#bits = ', num2str(nbits)]);
    end

    % check what radiobutton is selected in Cifra
    if get(handles.cifraVern, 'Value')
        [encoded_data, num_bits_to_append] = one_time_pad_cipher(encoded_data, [handles.path, handles.file]);
        handles.ciphdata = encoded_data;
        global bits_append;
        bits_append = num_bits_to_append;
        guidata(hObject, handles);
    end
    
    % check what radiobutton is selected in Codificação de canal
    if get(handles.codcanalBitPPar, 'Value')
        encoded_data = parity_check_encode(encoded_data, 7);
    elseif get(handles.codcanalH74, 'Value')
        encoded_data = hamming_encode(encoded_data);
    end
    
    input_bits = encoded_data;
    
    % check what radiobutton is selected in Codificação de linha /
    % Modelação digital
    if get(handles.nrzBipolar, 'Value')
        encoded_data = line_coder_NRZ_bipolar(bit_stuffing(encoded_data), 5, 10);
    elseif get(handles.nrzUnipolarMark, 'Value')
        encoded_data = line_coder_NRZ_unipolar_mark(bit_stuffing(encoded_data), 5, 10);
    elseif get(handles.ook, 'Value')
        encoded_data = ook_modulation_coder(bit_stuffing(encoded_data), 5, 4, 10);
    elseif get(handles.fsk, 'Value')
        encoded_data = fsk_modulation_coder(bit_stuffing(encoded_data), 5, 8, 2, 10);
    end
    
    global input_signal; % to plot after
    input_signal = encoded_data;
    
    
    set(handles.text22, 'string', ['#bits = ', num2str(length(encoded_data))]);
    guidata(hObject, handles);
    
% --- Decode RX data ----------------------------------------------------------
function decoded_data = decodeRX(hObject, eventdata, handles, encoded_data, code)
    decoded_data = encoded_data;
    global output_signal; % to plot after
    output_signal = decoded_data;
    
    
    % Line decoding / Digital demodulation
    if get(handles.nrzBipolar, 'Value')
        decoded_data = bit_unstuffing(line_decoder_NRZ_bipolar(decoded_data, 5, 10));
    elseif get(handles.nrzUnipolarMark, 'Value')
        decoded_data =  bit_unstuffing(line_decoder_NRZ_unipolar_mark(decoded_data, 5, 10));
    elseif get(handles.ook, 'Value')
        decoded_data =  bit_unstuffing(ook_modulation_decoder(decoded_data, 5, 4, 10));
    elseif get(handles.fsk, 'Value')
        decoded_data =  bit_unstuffing(fsk_modulation_decoder(decoded_data, 5, 8, 2, 10));
    end
    
    global output_bits;
    output_bits = decoded_data; 
    
    % Channel decoding
    if get(handles.codcanalBitPPar, 'Value')
        decoded_data = parity_check_decode(decoded_data, 8);
    elseif get(handles.codcanalH74, 'Value')
        decoded_data = hamming_decode_correct(decoded_data);
    end
    
    % Decipher
    if get(handles.cifraVern, 'Value')
        global bits_append;
        decoded_data = one_time_pad_decipher(decoded_data, [handles.path, handles.file], bits_append);
    end
    
    % Source decoding
    if get(handles.codfonteHuffman, 'Value')
        decoded_data = unhuffman(decoded_data, code);
    elseif get(handles.codfonteZIP, 'Value')
        decoded_data = unzipfile();
    end
    
%     global output_bits;
%     output_bits = decoded_data;
    
    guidata(hObject, handles);
           
% --- Executes on button press in abrirFileRX. ---------------------------------
function abrirFileRX_Callback(hObject, eventdata, handles)
    % hObject    handle to abrirFileRX (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    ext = extractAfter(handles.file, '.');
    fName = extractBefore(handles.file, '.');
    filename = strcat(handles.path, fName, '_RX.', ext);
    set_file_data(filename, handles.rxdata);
    open(filename);
    
% --- Executes on button press in fileHistogram. -------------------------------
function fileHistogram_Callback(hObject, eventdata, handles)
    % hObject    handle to fileHistogram (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)   
    plot_histogram(handles.data, handles.file);
    
% --- Executes on button press in cifraTextoClaro. -----------------------------
function cifraTextoClaro_Callback(hObject, eventdata, handles)
    % hObject    handle to cifraTextoClaro (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    fname = extractBefore(handles.file, '.');
    ext = extractAfter(handles.file, '.');
    open(strcat(handles.path, fname, '_ciphered.', ext));

% --- Executes on button press in cifraCriptograma. ----------------------------
function cifraCriptograma_Callback(hObject, eventdata, handles)
    % hObject    handle to cifraCriptograma (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)    
    filename = extractBefore(handles.file, '.');
    open([handles.path, filename, '.key']);

% --- Executes on button press in cifraTextoDecifrado. -------------------------
function cifraTextoDecifrado_Callback(hObject, eventdata, handles)
    % hObject    handle to cifraTextoDecifrado (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    fname = extractBefore(handles.file, '.');
    ext = extractAfter(handles.file, '.');
    open(strcat(handles.path, fname, '_deciphered.', ext));
    
function berVal_Callback(hObject, eventdata, handles)
    % hObject    handle to berVal (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'String') returns contents of berVal as text
    %        str2double(get(hObject,'String')) returns contents of berVal as a double

% --- Executes during object creation, after setting all properties. -----------
function berVal_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to berVal (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end



function noise_Callback(hObject, eventdata, handles)
% hObject    handle to noise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of noise as text
%        str2double(get(hObject,'String')) returns contents of noise as a double


% --- Executes during object creation, after setting all properties.
function noise_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function alfa_Callback(hObject, eventdata, handles)
% hObject    handle to alfa (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of alfa as text
%        str2double(get(hObject,'String')) returns contents of alfa as a double


% --- Executes during object creation, after setting all properties.
function alfa_CreateFcn(hObject, eventdata, handles)
% hObject    handle to alfa (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in graficar.
function graficar_Callback(hObject, eventdata, handles)
% hObject    handle to graficar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global input_signal;
global output_signal;
plot_channel(input_signal, output_signal);


% --- Executes on button press in plotNoise.
function plotNoise_Callback(hObject, eventdata, handles)
% hObject    handle to plotNoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global input_bits;
    global output_bits;
    snr = str2double(get(handles.noise, 'String'));
    if get(handles.fsk, 'Value')
        modulation = 'fsk';
    else 
        modulation = 'pam';
    end
    plot_noise(input_bits, output_bits, snr, modulation);
