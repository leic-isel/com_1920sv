%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% set_bits.m
%  
%

function data = set_bits( bit_data )
	% SET_BITS
    % Receives data from a bit vector
    % Returns the same data in 8bit integers

    p2 = 2.^(7:-1:0); % vector with the powers of 2
   
    % vector with 8 bit integer elements
    data = uint8( zeros(1, length(bit_data) / 8 ) );

    % point to point calculation
    k = 1;
    for i=1: length(data)
        data(i) = bit_data( k:k+7 ) * p2';
        k = k + 8;
    end  
end