function plot_channel(input_signal, output_signal)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
    cla reset;
    time = [0: 0.1: 32 - 0.1];
    plot(time, input_signal(1:320), 'k', time, output_signal(1:320), 'r');
    ylabel('x(t)');
    xlabel('Sinal');
    title('Efeito do canal no sinal recebido');
    legend('sinal emitido', 'sinal recebido');
end

