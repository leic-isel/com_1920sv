function [ output ] = fsk_modulation_coder( bits, amp, fbit1, fbit0, samples )
    %Frequency-Shift Keying modulation 
    % Input arguments 
    % amplitude = signal amplitude
    % freq1 = frequency to bit 1 
    % freq2 = frequency to bit 2
    % samples - given number of samples

    %creates the output array
    output = ones(1, length(bits)*samples);

    %Create an array with the size of length(bits) * samples for 
    %the time calculation. 
    %The step of t, is based on 1 divided by the number of samples.
    t = (0 : 1/samples : 1-(1/samples));

    %Carrier signal for bit = 1
    carrier1 = cos(2*pi*fbit1*t);
    %Carrier signal for bit = 0
    carrier0 = cos(2*pi*fbit0*t);

    %Bit inversor 
    invbits = xor(ones(1 , length(bits)), bits);

    index = 1;

    for i = 1 : samples : length(output)-(1/samples) 

        %Generate array for bits = 1
        ybit1(i : i+samples-1) = output(i : i+samples-1) .* bits(index);

        %Generate array for bits = 0, inverted 
        ybit0(i : i+samples-1) = output(i : i+samples-1) .* invbits(index);

        %Creates the wave for bit=1
        ybit1(i : i+samples-1) = ybit1(i : i+samples-1) .* carrier1;
        %Creates the wave for bit = 0, inverted
        ybit0(i : i+samples-1) = ybit0(i : i+samples-1) .* carrier0;
        index = index + 1;        
    end

    %Add both waves to the output
    output = ybit1 + ybit0; 

    %Multiplies the output for the amplitude
    output = output * amp;   
end

