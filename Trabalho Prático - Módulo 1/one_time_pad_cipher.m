%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunicações. 
%
% Trabalho Prático - Módulo 1
% 32398 Hélder Augusto
% 39619 Fábio Teixeira
% 45824 Nuno Venâncio
%
% one_time_pad_cipher.m
%  
%

function one_time_pad_cipher( filename )
	% ONE_TIME_PAD_CIPHER
    % Receives a filename to cipher
    % Saves the filename ciphered and the key to cipher / decipher

    % get bit data from file and imageSize in case of image file -------------- 
    bitvector = get_file_bits( filename );

    % array of random zeros and ones of length equal to the original bitvector 
    key = randi([0,1], 1, length(bitvector));
    
    % Save cipher / decipher key to a file ------------------------------------
    keyFileName = [extractBefore(filename, '.'), '.key'];
    write_file_data(keyFileName, key);
    
    % Cipher the file ---------------------------------------------------------
    ciphered = bitxor(bitvector, key); % XOR operate the bit_data with the key
    
    % Save the ciphered file --------------------------------------------------   
    name = extractBefore(filename, '.');
    ext = extractAfter (filename, '.');
    outFileName = [name, '_ciphered.', ext];
    write_file_data(outFileName, ciphered);
end


function bitvector = get_file_bits( filename )
    % GET_FILE_BITS
    % Receives a filename
    % Returns a bit vector containing the file data


    fileID = fopen( filename, 'rb' ); % open file for read
    
    if -1==fileID % Check descriptor.
        error('Error opening file %s for reading',filename);
    end

    data = fread( fileID, Inf, 'uchar' ); % read file to a vector
    
    fclose( fileID ); % close file
    
    data = data'; % Transpose to a row vector
    
    % get file bits
    k = 1;
    for i=1 : length(data)
        bitvector(k:k+7) = bitget( data(i), 8:-1:1 );
        k = k + 8;
    end
end



function write_file_data(filename, filedata)
    % WRITE_FILE_DATA
    % Receives filename and the filedata in bits
    % Saves the filedata to a file with the filename

    p2 = 2.^(7:-1:0); % vector with the powers of 2
   
    % vector with 8 bit integer elements
    ciphfile = uint8( zeros(1, length(filedata) / 8 ) );

    % point to point calculation
    k = 1;
    for i=1: length(ciphfile)
        ciphfile(i) = filedata( k:k+7 ) * p2';
        k = k + 8;
    end
    
    fileID = fopen( filename, 'wb'); % open file in write big Endian mode
    
    if -1==fileID % check descriptor
        error( 'Error opening file %s for writing ', filename );
    end
    
    fwrite( fileID, ciphfile, 'uchar' ); % write the vector to file

    fclose( fileID ); % close the file

end
